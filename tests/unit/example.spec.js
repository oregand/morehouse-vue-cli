import { shallowMount, flushPromises } from '@vue/test-utils'
import axios from 'axios';
import HelloWorld from '@/components/HelloWorld.vue'

describe('HelloWorld.vue', () => {
  const mockPokemonList = { data: { results: [
    { id: 1, name: 'unown' },
    { id: 2, name: 'wobbuffet' }
  ] } }
  
  jest.spyOn(axios, 'get').mockResolvedValue(mockPokemonList)

  it('renders props.msg when passed', () => {
    const msg = 'new message'
    const wrapper = shallowMount(HelloWorld, {
      propsData: { msg }
    })
    expect(wrapper.text()).toMatch(msg)
  })

  it('renders a list of 2 pokemon', async () => {
    const wrapper = shallowMount(HelloWorld)

    await wrapper.get('button').trigger('click');

    expect(axios.get).toHaveBeenCalledTimes(1)

    await flushPromises()

    expect(wrapper.findAll('[data-test="pokemon"]')).toHaveLength(2)
    expect(wrapper.findAll('[data-test="pokemon"]').at(0).text()).toBe('unown')
    expect(wrapper.findAll('[data-test="pokemon"]').at(1).text()).toBe('wobbuffet')
  })
})
